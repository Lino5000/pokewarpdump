# PokeWarpDump

*A very unimaginatively named project to dump the warp points from a Pokemon ROM*

# Credits

NDS ROM file system format retrieved from [this gbatek page](https://web.archive.org/web/20140429045759/http://nocash.emubase.de/gbatek.htm#dscartridgenitroromfilesystem).

Pokemon Platinum File structure pulled from [DSPRE](https://github.com/AdAstra-LD/DS-Pokemon-Rom-Editor)
