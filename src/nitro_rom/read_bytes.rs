use std::io::{ BufReader, Read };
use std::fs::File;
use byteorder::{ ByteOrder, LittleEndian };

macro_rules! read_bytes {
    ($reader:expr, $size:literal) => {
        {
            let mut buf = [0u8; $size];

            $reader.read_exact(&mut buf).unwrap();

            buf
        }
    };

    ($reader:expr, $size:expr) => {
        {
            let mut buf = Vec::with_capacity($size as usize);

            let mut part_reader = $reader.take($size as u64);

            part_reader.read_to_end(&mut buf).unwrap();

            buf
        }
    };
}
pub(crate) use read_bytes;

pub fn read_u8(reader: &mut BufReader<File>) -> u8 {
    let bytes = read_bytes!(reader, 1);
    bytes[0]
}

pub fn read_u16(reader: &mut BufReader<File>) -> u16 {
    LittleEndian::read_u16(&read_bytes!(reader, 2))
}

pub fn read_u32(reader: &mut BufReader<File>) -> u32 {
    LittleEndian::read_u32(&read_bytes!(reader, 4))
}

pub fn read_u64(reader: &mut BufReader<File>) -> u64 {
    LittleEndian::read_u64(&read_bytes!(reader, 8))
}

pub fn read_i8(reader: &mut BufReader<File>) -> i8 {
    let bytes = read_bytes!(reader, 1);
    bytes[0] as i8
}

pub fn read_i16(reader: &mut BufReader<File>) -> i16 {
    LittleEndian::read_i16(&read_bytes!(reader, 2))
}

pub fn read_i32(reader: &mut BufReader<File>) -> i32 {
    LittleEndian::read_i32(&read_bytes!(reader, 4))
}

pub fn read_i64(reader: &mut BufReader<File>) -> i64 {
    LittleEndian::read_i64(&read_bytes!(reader, 8))
}
