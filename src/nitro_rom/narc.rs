use byteorder::{ ByteOrder, LittleEndian };
use std::error::Error;
use std::io::{ Read, Seek, SeekFrom };

use super::read_bytes::read_bytes;
use super::file_system::{ dir_tree::*, data::NitroROMFATEntry };

struct NARCReader {
    data: Vec<u8>,
    cursor: usize,
}

impl NARCReader {
    fn new(data: Vec<u8>) -> Self {
        Self {
            data,
            cursor: 0
        }
    }

    fn current(&self) -> usize {
        self.cursor
    }

    fn read_u8(&mut self) -> u8 {
        read_bytes!(self, 1)[0]
    }

    fn read_u16(&mut self) -> u16 {
        LittleEndian::read_u16(&read_bytes!(self, 2))
    }

    fn read_u32(&mut self) -> u32 {
        LittleEndian::read_u32(&read_bytes!(self, 4))
    }

    fn read_u64(&mut self) -> u64 {
        LittleEndian::read_u64(&read_bytes!(self, 8))
    }
}

impl Read for NARCReader {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        let buf_size = buf.len();
        let read_len = std::cmp::min(self.data.len() - self.cursor, buf_size);

        for i in 0..read_len {
            buf[i] = self.data[self.cursor];
            self.cursor += 1;
        }

        Ok(read_len)
    }
}

impl Seek for NARCReader {
    fn seek(&mut self, pos: SeekFrom) -> std::io::Result<u64> {
        match pos {
            SeekFrom::Start(offset) => {
                if (offset as usize) < self.data.len() {
                    self.cursor = offset as usize;
                } else {
                    return Err(std::io::Error::new(std::io::ErrorKind::Other,
                            "Tried to seek outside file."));
                }
            },
            SeekFrom::End(offset) => {
                if offset >= 0 && (offset as usize) < self.data.len() {
                    self.cursor = self.data.len() - (offset as usize);
                } else {
                    return Err(std::io::Error::new(std::io::ErrorKind::Other,
                            "Tried to seek outside file."));
                }
            },
            SeekFrom::Current(offset) => {
                let new_cursor = self.cursor + offset as usize;
                if new_cursor < self.data.len() {
                    self.cursor = new_cursor;
                } else {
                    return Err(std::io::Error::new(std::io::ErrorKind::Other,
                            "Tried to seek outside file."));
                }
            },
        }

        Ok(self.cursor as u64)
    }
}

#[derive(Debug)]
#[allow(dead_code)]
struct NARCHeader {
    id: u32,
    id0: u16,
    id1: u16,
    file_size: u32,
    header_length: u16,
    num_sections: u16,
}

impl NARCHeader {
    fn parse(reader: &mut NARCReader) -> Self {
        Self {
            id: reader.read_u32(),
            id0: reader.read_u16(),
            id1: reader.read_u16(),
            file_size: reader.read_u32(),
            header_length: reader.read_u16(),
            num_sections: reader.read_u16(),
        }
    }
}

#[derive(Debug)]
struct NARCFATHeader {
    id: u32,
    length: u32,
    num_entries: u32,
}

impl NARCFATHeader {
    fn parse(reader: &mut NARCReader) -> Self {
        Self {
            id: reader.read_u32(),
            length: reader.read_u32(),
            num_entries: reader.read_u32(),
        }
    }
}

#[derive(Debug)]
struct NARCFATEntry {
    start: u32,
    end: u32,
}

impl NARCFATEntry {
    fn parse(reader: &mut NARCReader) -> Self {
        Self {
            start: reader.read_u32(),
            end: reader.read_u32(),
        }
    }
}

#[derive(Debug)]
struct NARCFNTHeader {
    _id: u32,
    length: u32,
    _unknown0: u32, // Can't find anything that knows wht these two fields are,
    unknown1: u32,  // but it seems that this one being == 8 indicates whether there is a FNT.
}

impl NARCFNTHeader {
    fn parse(reader: &mut NARCReader) -> Self {
        Self {
            _id: reader.read_u32(),
            length: reader.read_u32(),
            _unknown0: reader.read_u32(),
            unknown1: reader.read_u32(),
        }
    }
}

#[derive(Debug)]
struct NARCFNTEntry {
    _length: u8,
    name: String,
}

impl NARCFNTEntry {
    fn parse(reader: &mut NARCReader) -> Self {
        let length = reader.read_u8();
        Self {
            _length: length,
            name: String::from_utf8(read_bytes!(reader, length)).unwrap(),
        }
    }
}

#[derive(Debug)]
struct NARCFIMGHeader {
    _id: u32,
    _length: u32,
}

impl NARCFIMGHeader {
    fn parse(reader: &mut NARCReader) -> Self {
        Self {
            _id: reader.read_u32(),
            _length: reader.read_u32(),
        }
    }
}

pub struct NARC {
    name: String,
    reader: NARCReader,
}

impl NARC {
    pub fn new(name: &str, data: Vec<u8>) -> Self {
        Self { name: name.split('/').last().unwrap().to_string(), reader: NARCReader::new(data) }
    }

    pub fn unpack(&mut self, nitro_file: &NitroFile) -> Result<NitroDirectory, Box<dyn Error>> {
        // NARC Header
        let narc_header = NARCHeader::parse(&mut self.reader);

        // File Allocation Table
        let fat_header = NARCFATHeader::parse(&mut self.reader);

        //println!("{:?}", nitro_file);
        //println!("\t{:02X?}", self.reader.data);
        //println!("\t{:08X?}", narc_header);
        //println!("\t{:08X?}", fat_header);

        if fat_header.id != 0x46415442 {
            // Not a NARC file
            return Err(format!("The file `{}` does not contain NARC-format data.", nitro_file.name).into());
        }

        let mut fat: Vec<NARCFATEntry> = vec![];
        for _ in 0..fat_header.num_entries {
            fat.push(NARCFATEntry::parse(&mut self.reader));
        }

        // File Name Table
        let fnt_header = NARCFNTHeader::parse(&mut self.reader);
        
        let mut fnt: Vec<NARCFNTEntry> = vec![];
        if fnt_header.unknown1 == 8 {
            // Archive contains file names
            for _ in 0..fat_header.num_entries {
                fnt.push(NARCFNTEntry::parse(&mut self.reader));
            }
        } else {
            // Need to make new file names
            println!("NARC file `{}` doesn't contain any file names, making new ones.", self.name);
            for i in 0..fat_header.num_entries {
                //let name = format!("{}-{}", self.name, i);
                let name = format!("{}", i);
                fnt.push(NARCFNTEntry {
                    _length: name.len() as u8,
                    name
                });
            }
        }

        // Align to 4-byte word
        let seek_res = self.reader.seek(SeekFrom::Start(
                narc_header.header_length as u64 +
                fat_header.length as u64 +
                fnt_header.length as u64
        ));
        if let Err(_) = seek_res {
            panic!("The NARC file `{}` is malformed.", self.name);
        }

        // File Image
        let _fimg_header = NARCFIMGHeader::parse(&mut self.reader);

        let image_start = self.reader.current();
        let nitro_fimg_offset = nitro_file.address.start + image_start as u32;

        // Build the directory
        let mut out = NitroDirectory {
            name: self.name.clone(),
            id: nitro_file.address.id,
            files: vec![],
            sub_dirs: vec![]
        };

        for i in 0..fat_header.num_entries as usize {
            out.files.push(NitroFile {
                name: fnt[i].name.clone(),
                address: NitroROMFATEntry {
                    id: 0x0000, // How to determine an actual id?
                    start: nitro_fimg_offset + fat[i].start,
                    end: nitro_fimg_offset + fat[i].end
                }
            });
        }

        Ok(out)
    }
}
