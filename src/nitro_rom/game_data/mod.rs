pub mod event_file;
pub mod header_file;
pub mod text_archive;

pub mod prelude {
    pub use crate::nitro_rom::game_data::event_file::*;
    pub use crate::nitro_rom::game_data::header_file::*;
    pub use crate::nitro_rom::game_data::text_archive::*;
}
