use std::error::Error;
use std::fs::File;
use std::io::{ BufReader, Seek, SeekFrom };

use super::super::read_bytes::*;

#[derive(Debug, Default, Clone)]
pub struct HeaderFile {
    pub area_data_id: u8,
    pub unknown1: u8,
    pub matrix_id: u16,
    pub script_file_id: u16,
    pub level_script_id: u16,
    pub text_archive_id: u16,
    pub music_day_id: u16,
    pub music_night_id: u16,
    pub wild_pokemon: u16,
    pub event_file_id: u16,
    pub location_name: u8,
    pub area_icon: u8,
    pub weather_id: u8,
    pub camera_angle_id: u8,
    pub location_specifier: u8,
    pub battle_background: u8,
    pub flags: u8,
}

pub const MAP_HEADER_SIZE: u32 = 24;

impl HeaderFile {
    pub fn parse(reader: &mut BufReader<File>, base: u32) -> Result<Self, Box<dyn Error>> {
        reader.seek(SeekFrom::Start(base.into()))?;

        let mut out = Self::default();

        out.area_data_id = read_u8(reader);
        out.unknown1 = read_u8(reader);
        out.matrix_id = read_u16(reader);
        out.script_file_id = read_u16(reader);
        out.level_script_id = read_u16(reader);
        out.text_archive_id = read_u16(reader);
        out.music_day_id = read_u16(reader);
        out.music_night_id = read_u16(reader);
        out.wild_pokemon = read_u16(reader);
        out.event_file_id = read_u16(reader);
        out.location_name = read_u8(reader);
        out.area_icon = read_u8(reader);
        out.weather_id = read_u8(reader);
        out.camera_angle_id = read_u8(reader);

        let map_settings = read_u16(reader);
        out.location_specifier = (map_settings & 0b_1111_111) as u8;
        out.battle_background = ((map_settings >> 7) & 0b_111_1) as u8;
        out.flags = ((map_settings >> 12) & 0b_1111) as u8;

        Ok(out)
    }
}
