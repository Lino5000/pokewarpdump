use std::error::Error;
use std::fs::File;
use std::io::{ BufReader, Seek, SeekFrom };

use super::super::read_bytes::*;

#[derive(Debug, Default, Clone)]
pub struct WarpData {
    pub x_pos: i16,
    pub y_pos: i16,
    pub header: u16,
    pub anchor: u16,
    pub height: u32,
}

impl WarpData {
    fn parse(reader: &mut BufReader<File>) -> Self {
        Self {
            x_pos: read_i16(reader),
            y_pos: read_i16(reader),
            header: read_u16(reader),
            anchor: read_u16(reader),
            height: read_u32(reader),
        }
    }
}

const SPAWNABLE_SIZE: u32 = 0x14;
const OVERWORLD_SIZE: u32 = 0x20;

#[derive(Debug, Default, Clone)]
pub struct EventFile {
    pub warps: Vec<WarpData>,
}

impl EventFile {
    pub fn parse(reader: &mut BufReader<File>, base: u32) -> Result<Self, Box<dyn Error>> {
        reader.seek(SeekFrom::Start(base.into()))?;

        let mut event_file = Self::default();

        // Don't care about Spawnables
        let num_spawnables = read_u32(reader);
        reader.seek(SeekFrom::Current((num_spawnables * SPAWNABLE_SIZE).into()))?;

        // Don't care about Overworlds
        let num_overworlds = read_u32(reader);
        reader.seek(SeekFrom::Current((num_overworlds * OVERWORLD_SIZE).into()))?;

        let num_warps = read_u32(reader);
        for _ in 0..num_warps {
            event_file.warps.push(WarpData::parse(reader));
        }

        // Don't care about Triggers

        Ok(event_file)
    }
}
