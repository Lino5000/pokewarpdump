use std::collections::VecDeque;
use std::error::Error;
use std::fs::{ DirBuilder, File, OpenOptions };
use std::io::{ Write, BufReader };
use std::path::Path;
use regex::Regex;

use super::data::*;

#[derive(Debug, Clone)]
pub struct NitroFile {
    pub name: String,
    pub address: NitroROMFATEntry,
}

impl NitroFile {
    pub fn save_in_folder(&self, data: &mut BufReader<File>, folder_path: String) -> Result<(), Box<dyn Error>> {
        let content = self.address.get_content(data)?;
        let mut path = folder_path;
        path.push_str(&self.name);
        let mut file = OpenOptions::new().write(true).create(true).open(path)?;
        file.write_all(&content)?;
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct NitroDirectory {
    pub name: String,
    pub id: u16,
    pub files: Vec<NitroFile>,
    pub sub_dirs: Vec<NitroDirectory>,
}

impl NitroDirectory {
    fn print_tree_helper(&self, depth: u16, prefix: &str) {
        println!("{}> {}", prefix, self.name);
        if depth > 0 {
            let mut new_prefix = prefix.to_string();
            new_prefix.push_str("|   ");

            for sub_dir in self.sub_dirs.iter() {
                sub_dir.print_tree_helper(depth - 1, &new_prefix);
            }
            for file in self.files.iter() {
                println!("{}{}", new_prefix, file.name);
            }
        }
    }

    pub fn print_tree(&self, max_depth: u16) {
        self.print_tree_helper(max_depth, "");
    }

    pub fn build_dir_tree(rom: &NitroROM) -> NitroDirectory {
        fn build_dir(
            rom: &NitroROM,
            main_entry: (u16, u32, u16, u16),
            name: String,
        ) -> NitroDirectory {
            let (id, offset, _, _) = main_entry;

            if let Some(sub_table) = rom.fnt.sub_tables.get(&offset) {
                let mut files: Vec<NitroFile> = vec![];
                let mut sub_dirs: Vec<NitroDirectory> = vec![];

                for entry in sub_table.iter() {
                    if let NitroROMFNTEntry::SubDir(_, sub_name, id) = entry {
                        // Directory
                        sub_dirs.push(build_dir(
                            rom,
                            rom.fnt.main_table[(id - 0xF000) as usize],
                            sub_name.clone()
                        ));
                    }
                    if let NitroROMFNTEntry::File(_, sub_name, id) = entry {
                        // File
                        files.push(NitroFile {
                            name: sub_name.clone(),
                            address: rom.fat[*id as usize].clone(),
                        });
                    }
                }

                files.sort_by_key(|f| f.name.clone());

                NitroDirectory { name, id, files, sub_dirs }
            } else {
                panic!("There is no sub-table at the offset {}.", offset);
            }
        }

        build_dir(rom, rom.fnt.main_table[0], "root".to_string())
    }

    pub fn get_dir(&self, path: &str) -> Option<NitroDirectory> {
        fn helper(dir_tree: &NitroDirectory, mut path: VecDeque<&str>) -> Option<NitroDirectory> {
            let name = path.pop_front().unwrap();

            if path.is_empty() {
                if name == "" {
                    return Some(dir_tree.clone());
                } else {
                    // Path pointed to a file.
                    return None;
                }
            } else {
                // Sub-Dir
                for dir in dir_tree.sub_dirs.iter() {
                    if dir.name == name {
                        return helper(dir, path);
                    }
                }
            }

            None
        }

        helper(self, path.split('/').collect())
    }

    pub fn get_file(&self, path: &str) -> Option<NitroFile> {
        fn helper(dir_tree: &NitroDirectory, mut path: VecDeque<&str>) -> Option<NitroFile> {
            let name = path.pop_front().unwrap();

            if path.is_empty() {
                // File
                for file in dir_tree.files.iter() {
                    if file.name == name {
                        return Some(file.clone());
                    }
                }
            } else {
                // Sub-Dir
                for dir in dir_tree.sub_dirs.iter() {
                    if dir.name == name {
                        return helper(dir, path);
                    }
                }
            }

            None
        }

        helper(self, path.split('/').collect())
    }

    pub fn find(&self, pattern: &str, prefix: String) -> Vec<String> {
        let re: Regex = Regex::new(pattern).unwrap();
        let mut out: Vec<String> = vec![];

        let mut new_prefix = prefix.clone();
        new_prefix.push_str(&self.name);
        new_prefix.push('/');

        for file in self.files.iter() {
            if re.is_match(&file.name) {
                let mut path = new_prefix.clone();
                path.push_str(&file.name);
                out.push(path);
            }
        }

        for sub_dir in self.sub_dirs.clone() {
            out.append(&mut sub_dir.find(pattern, new_prefix.clone()));
        }

        out
    }

    pub fn is_file(&self, path: &str) -> bool {
        if let Some(_) = self.get_file(path) {
            true
        } else {
            false
        }
    }

    pub fn save_in_folder(&self, data: &mut BufReader<File>, parent_path: String) -> Result<(), Box<dyn Error>> {
        let mut path = parent_path;
        path.push_str(&self.name);
        path.push('/');

        DirBuilder::new().recursive(true).create(Path::new(&path))?;

        for file in self.files.iter() {
            file.save_in_folder(data, path.clone())?;
        }

        for dir in self.sub_dirs.iter() {
            dir.save_in_folder(data, path.clone())?;
        }

        Ok(())
    }
}


