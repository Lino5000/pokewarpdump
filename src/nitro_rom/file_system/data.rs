use byteorder::{ ByteOrder, LittleEndian };
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{ BufReader, Read, Seek, SeekFrom };

use super::super::read_bytes::read_bytes;

#[derive(Debug)]
pub enum NitroROMFNTEntry {
    // Entry format: (type/length, name, id)
    SubDir(u8, String, u16),
    File(u8, String, u16),
}

#[derive(Debug, Default)]
pub struct NitroROMFNT {
    // main table entries: (dir id, sub-table offset, sub-table first file id, parent dir id)
    // Root entry (id: 0xF000) has number of directories rather than parent id.
    pub main_table: Vec<(u16, u32, u16, u16)>,
    pub sub_tables: HashMap<u32, Vec<NitroROMFNTEntry>>,
}

impl NitroROMFNT {
    pub fn parse(reader: &mut BufReader<File>, base: u32) -> Result<Self, Box<dyn Error>> {
        let mut out = Self::default();
        
        reader.seek(SeekFrom::Start(base.into()))?;

        // Read Main table
        let root = (
            0xF000,
            LittleEndian::read_u32(&read_bytes!(reader, 4)),
            LittleEndian::read_u16(&read_bytes!(reader, 2)),
            LittleEndian::read_u16(&read_bytes!(reader, 2))
            );
        
        out.main_table.push(root);
        for id in 0xF001..(0xF000 + root.3) {
            out.main_table.push((
                    id,
                    LittleEndian::read_u32(&read_bytes!(reader, 4)),
                    LittleEndian::read_u16(&read_bytes!(reader, 2)),
                    LittleEndian::read_u16(&read_bytes!(reader, 2))
                    ));
        }

        // Read Sub-tables
        for (_id, offset, init_file_id, _) in out.main_table.iter() {
            reader.seek(SeekFrom::Start((base + offset).into()))?;

            let mut in_table = true;
            let mut file_id = (*init_file_id).clone();
            let mut sub_table: Vec<NitroROMFNTEntry> = vec![];

            while in_table {
                let type_and_length = read_bytes!(reader, 1)[0];

                if type_and_length != 0 {
                    let name_len = type_and_length & 0x7F;
                    let name = String::from_utf8(read_bytes!(reader, name_len))?;

                    sub_table.push(
                        if type_and_length >= 0x80 {
                            NitroROMFNTEntry::SubDir(
                                type_and_length,
                                name,
                                LittleEndian::read_u16(&read_bytes!(reader, 2))
                            )
                        } else {
                            file_id += 1;
                            NitroROMFNTEntry::File(
                                type_and_length,
                                name,
                                file_id - 1
                            )
                        }
                    );
                } else {
                    // End of sub-table
                    in_table = false;
                }
            }

            out.sub_tables.insert(*offset, sub_table);
        }

        Ok(out)
    }
}

#[derive(Debug, Clone)]
pub struct NitroROMFATEntry {
    pub id: u16,
    pub start: u32,
    pub end: u32,
}

impl NitroROMFATEntry {
    pub fn get_content(&self, reader: &mut BufReader<File>) -> Result<Vec<u8>, Box<dyn Error>> {
        reader.seek(SeekFrom::Start(self.start as u64))?;
        Ok(read_bytes!(reader, self.end - self.start))
    }
}

#[derive(Debug)]
pub struct NitroROM {
    pub fnt: NitroROMFNT,
    pub fat: Vec<NitroROMFATEntry>,
}
