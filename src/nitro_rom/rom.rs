use byteorder::{ ByteOrder, LittleEndian };
use std::collections::VecDeque;
use std::error::Error;
use std::fs::{ File, OpenOptions, remove_dir_all };
use std::io::{ BufReader, Read, Seek, SeekFrom };
use std::path::Path;

use super::{
    read_bytes::*,
    file_system::{
        data::*,
        dir_tree::{ NitroDirectory, NitroFile }
    },
    game_data::{
        header_file::{ HeaderFile, MAP_HEADER_SIZE },
        event_file::{ EventFile },
        text_archive::{ TextArchive }
    },
    narc::*
};

#[derive(Debug, Clone)]
#[allow(dead_code)]
pub struct RawHeader {
    title: [u8; 12],
    game_code: u32,
    maker_code: u16,
    unit_code: u8,
    enc_seed_select: u8,
    dev_cap: u8,
    // reserved, 9 bytes
    rom_version: u8,
    autostart: u8,
    arm9_rom_offset: u32,
    arm9_entry_addr: u32,
    arm9_ram_addr: u32,
    arm9_size: u32,
    arm7_rom_offset: u32,
    arm7_entry_addr: u32,
    arm7_ram_addr: u32,
    arm7_size: u32,
    fnt_offset: u32,
    fnt_size: u32,
    fat_offset: u32,
    fat_size: u32,
    arm9_overlay_offset: u32,
    arm9_overlay_size: u32,
    arm7_overlay_offset: u32,
    arm7_overlay_size: u32,
    gamecard_bus_control_setting_normal: u32,
    gamecard_bus_control_setting_key1: u32,
    icon_title_offset: u32,
    secure_crc: u16,
    secure_load_timeout: u16,
    arm9_autoload_ram_addr: u32,
    arm7_autoload_ram_addr: u32,
    secure_disable: u64,
    rom_total_size: u32,
    rom_header_size: u32,
    // reserved 0x38 bytes
    nintendo_logo: [u8; 0x9C],
    nintendo_logo_crc: u16,
    header_crc: u16,
    debug_rom_offset: u32,
    debug_size: u32,
    debug_ram_addr: u32
    // reserved 4 bytes
    // reserved 0x90 bytes
}

impl Default for RawHeader {
    fn default() -> Self {
        Self {
            title: [0; 12],
            game_code: 0,
            maker_code: 0,
            unit_code: 0,
            enc_seed_select: 0,
            dev_cap: 0,
            rom_version: 0,
            autostart: 0,
            arm9_rom_offset: 0,
            arm9_entry_addr: 0,
            arm9_ram_addr: 0,
            arm9_size: 0,
            arm7_rom_offset: 0,
            arm7_entry_addr: 0,
            arm7_ram_addr: 0,
            arm7_size: 0,
            fnt_offset: 0,
            fnt_size: 0,
            fat_offset: 0,
            fat_size: 0,
            arm9_overlay_offset: 0,
            arm9_overlay_size: 0,
            arm7_overlay_offset: 0,
            arm7_overlay_size: 0,
            gamecard_bus_control_setting_normal: 0,
            gamecard_bus_control_setting_key1: 0,
            icon_title_offset: 0,
            secure_crc: 0,
            secure_load_timeout: 0,
            arm9_autoload_ram_addr: 0,
            arm7_autoload_ram_addr: 0,
            secure_disable: 0,
            rom_total_size: 0,
            rom_header_size: 0,
            nintendo_logo: [0; 0x9C],
            nintendo_logo_crc: 0,
            header_crc: 0,
            debug_rom_offset: 0,
            debug_size: 0,
            debug_ram_addr: 0
        }
    }
}

impl RawHeader {
    pub fn from_bufreader(mut reader: &mut BufReader<File>) -> Result<Self, Box<dyn Error>> {
        Ok(Self {
            title: read_bytes!(reader, 12),
            game_code: read_u32(&mut reader),
            maker_code: read_u16(&mut reader),
            unit_code: read_u8(&mut reader),
            enc_seed_select: read_u8(&mut reader),
            dev_cap: read_u8(&mut reader),
            // reserved, 9 bytes
            rom_version: {read_bytes!(reader, 9); read_u8(&mut reader)},
            autostart: read_u8(&mut reader),
            arm9_rom_offset: read_u32(&mut reader),
            arm9_entry_addr: read_u32(&mut reader),
            arm9_ram_addr: read_u32(&mut reader),
            arm9_size: read_u32(&mut reader),
            arm7_rom_offset: read_u32(&mut reader),
            arm7_entry_addr: read_u32(&mut reader),
            arm7_ram_addr: read_u32(&mut reader),
            arm7_size: read_u32(&mut reader),
            fnt_offset: read_u32(&mut reader),
            fnt_size: read_u32(&mut reader),
            fat_offset: read_u32(&mut reader),
            fat_size: read_u32(&mut reader),
            arm9_overlay_offset: read_u32(&mut reader),
            arm9_overlay_size: read_u32(&mut reader),
            arm7_overlay_offset: read_u32(&mut reader),
            arm7_overlay_size: read_u32(&mut reader),
            gamecard_bus_control_setting_normal: read_u32(&mut reader),
            gamecard_bus_control_setting_key1: read_u32(&mut reader),
            icon_title_offset: read_u32(&mut reader),
            secure_crc: read_u16(&mut reader),
            secure_load_timeout: read_u16(&mut reader),
            arm9_autoload_ram_addr: read_u32(&mut reader),
            arm7_autoload_ram_addr: read_u32(&mut reader),
            secure_disable: read_u64(&mut reader),
            rom_total_size: read_u32(&mut reader),
            rom_header_size: read_u32(&mut reader),
            // reserved 0x38 bytes
            nintendo_logo: {read_bytes!(reader, 0x38); read_bytes!(reader, 0x9C)},
            nintendo_logo_crc: read_u16(&mut reader),
            header_crc: read_u16(&mut reader),
            debug_rom_offset: read_u32(&mut reader),
            debug_size: read_u32(&mut reader),
            debug_ram_addr: read_u32(&mut reader)
            // reserved 4 bytes
            // reserved 0x90 bytes
        })
    }
}
#[derive(Debug)]
pub struct ROM {
    // Just the important info
    pub reader: BufReader<File>,
    header: RawHeader,
    dir_tree: Option<NitroDirectory>,
    pub file_system: NitroROM,
}

const EVENT_FILES_PATH: &str = "fielddata/eventdata/zone_event.narc";
const TEXT_ARCHIVES_PATH: &str = "msgdata/pl_msg.narc";
const INTERNAL_NAMES_PATH: &str = "fielddata/maptable/mapname.bin";
pub const NUM_HEADERS: u16 = 593;


impl ROM {
    pub fn load_nds_file(filepath: &str) -> Result<Self, Box<dyn Error>> {
        let file = OpenOptions::new().read(true).open(filepath)?;
        let reader = BufReader::new(file);

        let mut nds_rom = Self {
            reader,
            header: RawHeader::default(),
            dir_tree: None,
            file_system: NitroROM {
                fnt: NitroROMFNT::default(),
                fat: vec![],
            },
        };

        nds_rom.parse_reader()?;

        nds_rom.dir_tree = Some(NitroDirectory::build_dir_tree(&nds_rom.file_system));

        Ok(nds_rom)
    }

    fn parse_reader(&mut self) -> Result<(), Box<dyn Error>> {
        self.header = RawHeader::from_bufreader(&mut self.reader)?;

        // FNT
        self.file_system.fnt = NitroROMFNT::parse(&mut self.reader, self.header.fnt_offset)?;

        // FAT
        self.reader.seek(SeekFrom::Start(self.header.fat_offset as u64))?;
        for id in (0 as u16)..(self.header.fat_size / 8) as u16 {
            self.file_system.fat.push(NitroROMFATEntry {
                id,
                start: LittleEndian::read_u32(&read_bytes!(self.reader, 4)),
                end: LittleEndian::read_u32(&read_bytes!(self.reader, 4)),
            });
        }

        Ok(())
    }

    pub fn get_dir_tree(&self) -> NitroDirectory {
        self.dir_tree.clone().unwrap()
    }

    pub fn get_file(&self, path: &str) -> Option<NitroFile> {
        self.get_dir_tree().get_file(path)
    }

    pub fn get_dir(&self, path: &str) -> Option<NitroDirectory> {
        self.get_dir_tree().get_dir(path)
    }

    pub fn get_dir_mut<'a>(&'a mut self, path: &str) -> Option<&'a mut NitroDirectory> {
        fn helper<'b>(
            dir_tree: &'b mut NitroDirectory,
            mut path: VecDeque<&str>
        ) -> Option<&'b mut NitroDirectory> {
            let name = path.pop_front().unwrap();

            if path.is_empty() {
                if name == "" {
                    return Some(dir_tree);
                } else {
                    // Path pointed to a file.
                    return None;
                }
            } else {
                // Sub-Dir
                for dir in dir_tree.sub_dirs.iter_mut() {
                    if dir.name == name {
                        return helper(dir, path);
                    }
                }
            }

            None
        }

        helper(self.dir_tree.as_mut().unwrap(), path.split('/').collect())
    }

    pub fn read_file(&mut self, file: NitroFile) -> Result<Vec<u8>, Box<dyn Error>> {
        Ok(file.address.get_content(&mut self.reader)?)
    }

    pub fn read_file_path(&mut self, path: &str) -> Result<Vec<u8>, Box<dyn Error>> {
        if let Some(file) = self.get_file(path) {
            Ok(file.address.get_content(&mut self.reader)?)
        } else {
            Err("The requested file is not contained in any sub-directories of this directory.")?
        }
    }

    pub fn extract_narc(&mut self, filepath: &str) -> Result<(), Box<dyn Error>> {
        if let Some(file) = self.get_file(filepath) {
            let archive = NARC::new(filepath, self.read_file(file.clone())?).unpack(&file)?;
            
            let mut split_path = filepath.rsplitn(2, '/');
            let filename = split_path.next().unwrap();
            let mut folder_path = split_path.next().unwrap().to_string();
            folder_path.push('/');

            let folder = self.get_dir_mut(&folder_path).unwrap();
            println!("{}: {:?}", folder_path, folder.files.iter().map(|f| &f.name).collect::<Vec<&String>>());

            let search_res = folder.files.binary_search_by_key(&filename, |file| &file.name);
            println!("{:?}", search_res);

            if let Ok(file_index) = search_res {
                if let Err(folder_index) = folder.sub_dirs.binary_search_by_key(&filename, |dir| &dir.name) {
                    folder.files.remove(file_index);
                    folder.sub_dirs.insert(folder_index, archive);
                } else {
                    return Err(
                        format!("The file `{}` couldn't be extracted; {}",
                            filepath, "there is already a folder by that name.").into()
                    );
                }

                Ok(())
            } else {
                Err(format!("The file `{}` doesn't have an index.", filepath).into())
            }
        } else {
            Err(format!("The file `{}` does not exist.", filepath).into())
        }
    }

    pub fn unpack_all_narc_files(&mut self) -> Result<(), Box<dyn Error>>{
        let narc_paths_tmp = self.dir_tree.clone().unwrap()
            .find(r".*\.narc$", "".to_string());
        let narc_paths = narc_paths_tmp.iter()
            .map(|path| path.splitn(2, '/').last().unwrap().to_string());
        for narc_path in narc_paths {
            if let Err(e) = self.extract_narc(&narc_path) {
                println!("{}", e);
            }
        }

        Ok(())
    }

    pub fn save_file_system(mut self, base_dir: &str) -> Result<(), Box<dyn Error>> {
        remove_dir_all(Path::new(base_dir))?;
        if let Some(nitro_dir) = self.dir_tree {
            nitro_dir.save_in_folder(&mut self.reader, String::from(base_dir))
        } else {
            Err(
                format!("Could not save extracted ROM; It does not have a directory tree yet.").into()
            )
        }
    }

    fn headers_start(&self) -> u32 {
        match self.header.game_code {
            0x45555043 => 0xE601C,
            0x53555043 => 0xE60B0,
            0x49555043 => 0xE6038,
            0x46555043 => 0xE60A4,
            0x44555043 => 0xE6074,
            0x4a555043 => 0xE56F0,
            _ => {
                panic!("I don't know how to deal with a rom that has game_code = {:#X}!", self.header.game_code);
            }
        }
    }

    pub fn load_header(&mut self, header_number: u16) -> Result<HeaderFile, Box<dyn Error>> {
        let header_offset = self.header.arm9_rom_offset + self.headers_start() + MAP_HEADER_SIZE * (header_number as u32);
        HeaderFile::parse(&mut self.reader, header_offset)
    }

    pub fn load_event_file(&mut self, event_file_number: u16) -> Result<EventFile, Box<dyn Error>> {
        if self.get_dir_tree().is_file(EVENT_FILES_PATH) {
            self.extract_narc(EVENT_FILES_PATH)?;
        }
        if let Some(fat_entry) = self.get_file(&format!("{}/{}", EVENT_FILES_PATH, event_file_number)) {
            EventFile::parse(&mut self.reader, fat_entry.address.start)
        } else {
            Err(format!("There is no Event File number {}.", event_file_number).into())
        }
    }

    pub fn load_text_archive(&mut self, text_archive_number: u16) -> Result<TextArchive, Box<dyn Error>> {
        if self.get_dir_tree().is_file(TEXT_ARCHIVES_PATH) {
            self.extract_narc(TEXT_ARCHIVES_PATH)?;
        }
        if let Some(fat_entry) = self.get_file(&format!("{}/{}", TEXT_ARCHIVES_PATH, text_archive_number)) {
            TextArchive::parse(&mut self.reader, fat_entry.address.start)
        } else {
            Err(format!("There is no Text Archive number {}.", text_archive_number).into())
        }
    }

    pub fn get_internal_names(&mut self) -> Result<Vec<String>, Box<dyn Error>> {
        if self.get_dir_tree().is_file(INTERNAL_NAMES_PATH) {
            if let Some(fat_entry) = self.get_file(INTERNAL_NAMES_PATH) {
                let mut names: Vec<String> = Vec::new();
                let base = fat_entry.address.start;
                let mut offset = 0;

                while base + offset < fat_entry.address.end {
                    self.reader.seek(SeekFrom::Start((base + offset).into()))?;

                    let mut name = String::new();

                    for _ in 0..16 {
                        let car = read_u8(&mut self.reader);
                        if car == 0 {
                            break;
                        }
                        name.push(car as char);
                    }
                    names.push(name);

                    offset += 16;
                }

                Ok(names)
            } else {
                Err(format!("The Internal Names File is not where I expected: {}", INTERNAL_NAMES_PATH).into())
            }
        } else {
            Err(format!("The Internal Names File is not where I expected: {}", INTERNAL_NAMES_PATH).into())
        }
    }
}
