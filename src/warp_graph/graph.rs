use std::collections::HashMap;
use std::error::Error;

use crate::nitro_rom::{
    prelude::{ ROM, NUM_HEADERS },
    game_data::text_archive::TextArchive,
    game_data::event_file::WarpData,
};

fn dot_sanitize(s: &str) -> String {
    s.replace(' ', "_")
        .replace(['.', '\''], "")
        .replace('é', "e")
}

#[derive(Debug, Default)]
pub struct WarpPoint {
    pub data: WarpData,
    pub header_id: usize, // Index into the Graph's headers partition.
    pub id: usize, // WarpPoint ids are indices into the Graph's warp_points Vec.
    pub target_id: usize, // WarpPoint ids are indices into the Graph's warp_points Vec.
    pub outgoing_links: Vec<usize>, // Indices into the Graph's warp_links Vec.
}

#[derive(Debug, PartialEq)]
pub enum WarpType {
    Warp,
    SameMap,
    ConnectMaps,
}

impl Default for WarpType {
    fn default() -> Self {
        Self::Warp
    }
}

#[derive(Debug, Default)]
pub struct LinkProperties {
    pub warp_type: WarpType,
}

#[derive(Debug, Default)]
pub struct WarpLink {
    pub from_id: usize, // WarpPoint ids are indexes into the Graph's warp_points Vec.
    pub to_id: usize, // WarpPoint ids are indexes into the Graph's warp_points Vec.
    pub properties: LinkProperties,
}

#[derive(Debug, Default)]
pub struct Graph {
    pub warp_points: Vec<WarpPoint>, // All the warp points
    pub warp_links: Vec<WarpLink>, // All the links between warp points
    // Display Name, Internal Name, Contained Warps
    pub headers: Vec<(String, String, Vec<usize>)>,
    pub headers_by_display_name: HashMap<String, Vec<usize>>, // Partitions the headers by display name
    pub header_file_to_index: HashMap<u16, usize>,
}

impl Graph {
    // Returns the WarpPoint id that is assigned to the warp point.
    pub fn add_warp_point(&mut self, warp: WarpData) -> usize {
        let id = self.warp_points.len();
        self.warp_points.push(WarpPoint {
            data: warp,
            id: id,
            ..Default::default()
        });

        id
    }

    pub fn add_header(&mut self, header_name: &str, internal_name: &str) -> usize {
        let id = self.headers.len();
        self.headers.push((header_name.into(), internal_name.into(), Vec::new()));

        if let Some(header_collection) = self.headers_by_display_name.get_mut(header_name.into()) {
            header_collection.push(id);
        } else {
            self.headers_by_display_name.insert(header_name.into(), vec![id]);
        }

        id
    }

    pub fn add_warp_to_header(&mut self, header_id: usize, warp_id: usize) {
        self.headers[header_id].2.push(warp_id);
        self.warp_points[warp_id].header_id = header_id;
    }

    // Returns the WarpLink id that is assigned to the created warp link.
    pub fn add_link(&mut self, from_id: usize, to_id: usize) -> usize {
        let id = self.warp_links.len();
        self.warp_links.push(WarpLink {
            from_id, to_id,
            properties: LinkProperties::default(),
        });
        self.warp_points[from_id].outgoing_links.push(id);

        id
    }

    pub fn build_from_rom(rom: &mut ROM) -> Result<Self, Box<dyn Error>> {
        fn add_nodes(
            location_names: &TextArchive, internal_names: &Vec<String>,
            graph: &mut Graph, rom: &mut ROM, header_id: u16
        ) -> Result<(), Box<dyn Error>> {
            if let Some(_) = graph.header_file_to_index.get(&header_id) {
                Ok(())
            } else if header_id > NUM_HEADERS {
                eprintln!("\n\nCan't load nodes from header {}", header_id);
                Ok(())
            } else {
                eprintln!("\n\nLoading nodes from header {}", header_id);
                let header_file = rom.load_header(header_id)?;
                let header_name = &location_names.messages[header_file.location_name as usize];
                let internal_name = &internal_names[header_id as usize];
                eprintln!("\nHeader: {} - {}\n{:?}", header_name, internal_name, header_file.clone());

                let event_file = rom.load_event_file(header_file.event_file_id)?;
                eprintln!("\nEvents: {:?}", event_file.clone());

                let graph_header_id = graph.add_header(header_name, internal_name);
                eprintln!("\nGraph Header: {}", graph_header_id);

                assert_eq!(graph.header_file_to_index.insert(header_id, graph_header_id), None);

                for warp_data in event_file.warps {
                    let target_header_id = warp_data.header;
                    let warp_id = graph.add_warp_point(warp_data);
                    graph.add_warp_to_header(graph_header_id, warp_id);

                    add_nodes(
                        location_names, internal_names,
                        graph, rom, target_header_id
                    )?;
                }

                eprintln!("\n\nFinished with header {}", header_id);

                Ok(())
            }
        }

        let mut out = Self::default();

        // Get name lists
        let location_names = rom.load_text_archive(433)?;
        let internal_names = rom.get_internal_names()?;

        add_nodes(
            &location_names, &internal_names,
            &mut out, rom,
            //411 // Twinleaf Town Outside
            3 // Jubilife City Outside
        )?;

        // Build links
        let mut links: Vec<(usize, usize)> = Vec::new();
        for warp in out.warp_points.iter() {
            if let Some(&target_header) = out.header_file_to_index.get(&warp.data.header) {
                let target_warp = out.headers[target_header].2[warp.data.anchor as usize];
                links.push((warp.id, target_warp));
            }
        }
        for link in links.iter() {
            out.add_link(link.0, link.1);
        }

        out.add_links_inside_headers();

        Ok(out)
    }

    pub fn add_links_inside_headers(&mut self) {
        let mut links: Vec<(usize, usize)> = Vec::new();
        for (_, _, header_warps) in self.headers.iter() {
            for &start_point in header_warps.iter() {
                for &end_point in header_warps.iter() {
                    if start_point != end_point {
                        links.push((start_point, end_point));
                    }
                }
            }
        }
        for (start_point, end_point) in links.iter() {
            let link_id = self.add_link(*start_point, *end_point);
            self.warp_links[link_id].properties.warp_type = WarpType::SameMap;
        }
    }

    fn warp_point_name(&self, warp_id: usize) -> String {
        let warp_point = &self.warp_points[warp_id];
        format!("{} {} {}",
            self.headers[warp_point.header_id].1,
            warp_point.data.x_pos,
            warp_point.data.y_pos,
        )
    }

    fn warp_link_formatting(&self, props: &LinkProperties) -> String {
        let head_shape = match props.warp_type {
            WarpType::Warp => "normal",
            _ => "ornormal",
        };

        format!(" [arrowhead={}]", head_shape)
    }

    pub fn dump_graphviz(&self, cluster: bool, draw_internal_edges: bool) -> String {
        let mut out = String::new();

        // Graphviz Header
        out.push_str("digraph WarpGraph {\n");

        // Graphviz Nodes are WarpPoints
        if cluster {
            // Cluster the nodes based on Header Name
            for (cluster_display_name, headers) in self.headers_by_display_name.iter() {
                out.push_str(&format!("    subgraph cluster{}", dot_sanitize(cluster_display_name)));
                out.push_str(" {\n");
                out.push_str(&format!("        label=\"{}\"\n", cluster_display_name));
                for header_id in headers {
                    let (display_name, name, warps) = &self.headers[*header_id];
                    out.push_str(&format!("        subgraph cluster{}", name));
                    out.push_str(" {\n");
                    out.push_str(&format!("            label=\"{} - {}\"\n", display_name, name));

                    for warp in warps.iter() {
                        let warp_point = &self.warp_points[*warp];
                        out.push_str(&format!("            {} [label=\"{}\"]\n",
                                self.warp_point_name(warp_point.id).replace(" ", "_"),
                                self.warp_point_name(warp_point.id),
                        ));
                    }

                    out.push_str("        }\n");
                }
                out.push_str("    }\n");
            }
        } else {
            for warp_point in self.warp_points.iter() {
                out.push_str(&format!("    {} [label=\"{}\"]\n",
                        self.warp_point_name(warp_point.id).replace(" ", "_"),
                        self.warp_point_name(warp_point.id),
                ));
            }
        }

        // Graphviz Edges are WarpLinks
        for warp_link in self.warp_links.iter() {
            if draw_internal_edges || (warp_link.properties.warp_type != WarpType::SameMap) {
                out.push_str(&format!("    {}->{}{}\n",
                        self.warp_point_name(warp_link.from_id).replace(" ", "_"),
                        self.warp_point_name(warp_link.to_id).replace(" ", "_"),
                        self.warp_link_formatting(&warp_link.properties),
                ));
            }
        }

        // Graphviz Footer
        out.push_str("}\n");

        out
    }
}
