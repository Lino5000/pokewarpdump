#![allow(dead_code)]

use std::error::Error;

use std::fs::OpenOptions;
use std::io::Write;

mod nitro_rom;
use nitro_rom::prelude::{ ROM };

mod warp_graph;
use warp_graph::prelude::{ Graph };

fn main() -> Result<(), Box<dyn Error>> {
    let mut rom = ROM::load_nds_file("Pokemon Platinum.nds")?;

    let warp_graph = Graph::build_from_rom(&mut rom)?;

    //println!("{}", warp_graph.dump_graphviz());
    let mut file = OpenOptions::new()
        .write(true)
        .truncate(true)
        .create(true)
        .open("warp_graph.dot")?;
    write!(file, "{}", warp_graph.dump_graphviz(true, false))?;

    Ok(())
}
